<?php
/**
 * Template Name: Album
 *
 * @package WordPress
 */
 ?>
 <html <?php language_attributes(); ?>>
 	<head>
 		<title><?php wp_title(); ?></title>
 		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
 		<?php wp_head(); ?>
 	</head>
 	<body>
        <header>
            <div id="logo">
				<a href="/">
					<img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/logo-m-1.png" alt="KZBytów" />
				</a>
			</div>
			<nav class="header_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
			<img id="showMobileMenu" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/menu.png"/>
		</header>
        <div id="menuBottomLine"></div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="submenu">
                <a href="/album">Galeria</a> > <?php the_title(); ?>
            </div>
        <h2><?php the_title(); ?></h2>
        <p class="subheading">
            Kliknij zdjęcie, aby powiększyć
        </p>
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php endif; ?>
        <br style="clear: both">
        <p id="galleryP" class="subheading">
            Więcej zdjęć można zobaczyć na naszej stronie <a target="_blank" href="https://www.facebook.com/kzbytow/photos">Facebook</a>
        </p>

<?php get_footer(); ?>
