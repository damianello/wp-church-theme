<?php
/**
 * Template Name: Galeria
 *
 * @package WordPress
 */
 ?>
 <html <?php language_attributes(); ?>>
 	<head>
 		<title><?php wp_title(); ?></title>
 		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
 		<?php wp_head(); ?>
 	</head>
 	<body>
        <header>
            <div id="logo">
				<a href="/">
					<img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/logo-m-1.png" alt="KZBytów" />
				</a>
			</div>
			<nav class="header_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
			<img id="showMobileMenu" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/menu.png"/>
		</header>
        <div id="menuBottomLine"></div>
    <div id="content">
        <section id="galeria">
            <h2>Galeria</h2>
            <p class="subheading">
                Kliknij w okładkę albumu, żeby zobaczyć więcej zdjęć.
            </p>
            <div id="galleryBox">
                <?php
                    $args = array('post_type' => 'galeria', 'order' => 'ASC');

                    $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                    if ( $top_posts->have_posts() ) :
                        $i = 1;
                        while ( $top_posts->have_posts() ) : $top_posts->the_post();

                            ?>
                                <a target="_blank" class="galleryItemA" href="<?php the_field('link'); ?>">
                                    <div class="galleryItem">
                                        <?php the_post_thumbnail(); ?>
                                        <div class="galleryItemTitle">
                                            <?php the_title(); ?>
                                        </div>
                                    </div>
                                </a>
                            <?php
                            $i++;
                        endwhile;
                    endif;
                ?>
            </div>
            <a href="/album">
                <p class="subheading">
                    więcej...
                </p>
            </a>
        </section>
    </content>

<?php get_footer(); ?>
