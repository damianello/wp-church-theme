<?php
/**
 * Template Name: News
 *
 * @package WordPress
 */
 ?>
 <html <?php language_attributes(); ?>>
 	<head>
 		<title><?php wp_title(); ?></title>
 		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
 		<?php wp_head(); ?>
 	</head>
 	<body>
        <header>
            <div id="logo">
				<a href="/">
					<img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/logo-m-1.png" alt="KZBytów" />
				</a>
			</div>
			<nav class="header_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
			<img id="showMobileMenu" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/menu.png"/>
		</header>
        <div id="menuBottomLine"></div>
    <div id="content">
        <section id="galeria">
            <h2>Video</h2>
            <p class="subheading">
                OBEJRZYJ NAS NA YOUTUBE
            </p>
            <div id="galleryBoxP">
                <?php
                    $args = array('post_type' => 'news', 'order' => 'ASC', 'posts_per_page' => "3");

                    $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                    if ( $top_posts->have_posts() ) :
                        $i = 1;
                        while ( $top_posts->have_posts() ) : $top_posts->the_post();
                            ?>
                                <a target="_blank" href="<?php the_field('link'); ?>">
                                    <div class="newsItem">
                                        <?php the_post_thumbnail(); ?>
                                        <div class="newsOverlay"></div>
                                        <p class="newsTitle"><?php the_title(); ?></p>
                                        <?php the_content(); ?>
                                        <?php the_field('title'); ?>
                                    </div>
                                </a>
                            <?php
                            $i++;
                        endwhile;
                    endif;
                ?>
            </div>

        </section>
    </content>

<?php get_footer(); ?>
