'use strict';

$(window).ready(function() {

    // $('.slidee').children('li').children('img').click(function() {
    //     window.open('https://www.youtube.com/channel/UCxiCpDBJ3Zo_jPBNrRehnXA', '_blank');
    // });

    $(document).on("scroll", onScroll);

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        for (var i = $('.menu-header-menu-container > ul > li > a').length; i <= 6; i++) {
            let currLink = $('.menu-header-menu-container > ul > li > a').get(i);
            let refElement = current.attr('href');
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.menu-header-menu-container ul li a').removeClass("active");
                currLink.addClass("active");
            } else {
                currLink.removeClass("active");
            }
        }
    }

    $('.newsItem').hover(function() {
        $(this).children('.newsOverlay').css('display', 'block');
    }, function() {
        $(this).children('.newsOverlay').css('display', 'none');
    });

    var nav = $("nav");
    var mns = "main-nav-scrolled";
    var hdr = $('header').height();
    var winW = $(window).width();

    $(window).scroll(function() {
        if (winW > 1240) {
            if ($(this).scrollTop() > hdr) {
                nav.addClass(mns);
                $('.menu-header-menu-container').css('height', '50px');
            } else {
                nav.removeClass(mns);
                $('.menu-header-menu-container').css('height', '100px');
            }
        }
    });

    $('nav > div > ul > li > a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 70
                }, 1000);
                return false;
            }
        }
    });

    $('.gallery').find('br').remove();

    $('#onas ul li').on('click', function() {
        setAboutBgHeight();
    });

    setAboutBgHeight();

    function setAboutBgHeight() {
        let aboutHeightHe = $('.about_element').filter(function() {
            return $(this).css('display') == 'block';
        }).height();
        let aboutBgEl = $('.about_element').filter(function() {
            return $(this).css('display') == 'block';
        }).children('.aboutBg');
        // let aboutHeightHe = $('.about_element[style*="display:block"]').css('height');
        aboutBgEl.css('height', aboutHeightHe);
        console.log(aboutHeightHe);
        console.log(aboutBgEl);
    }

    $('.galleryItem').hover(function() {
        $(this).children('.galleryItemTitle').css('display', 'flex');
        $(this).children('img').addClass('blured');
    }, function() {
        $(this).children('.galleryItemTitle').css('display', 'none');
        $(this).children('img').removeClass('blured');
    });

    $('.galleryAllItem').hover(function() {
        $(this).children('.galleryAllItemTitle').css('display', 'flex');
        $(this).children('img').addClass('blured');
    }, function() {
        $(this).children('.galleryAllItemTitle').css('display', 'none');
        $(this).children('img').removeClass('blured');
    });


    function coversSetWidth() {
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var cntctW = $(window).width() / 3;

        if (winW < 1280) {
            var topCoverImg = winW / 2 - 30;
            var topCoverImgH = topCoverImg / 1.57;
            var bottomCoverImg = winW - 80;
            var bottomCoverImgT = bottomCoverImg / 3;
            var ksH = winW / 1.85;
            var ksHW = ksH + 100;

            $('#topCovers > .coverImg').css('width', topCoverImg);
            $('#topCovers > .coverImg').css('height', topCoverImgH);
            $('#bottomCovers > .coverImg').css('width', bottomCoverImgT);
            $('#covers').css('height', ksHW);
            var aboutHeight = $('.aboutText').height();
            // var heightONAS = aboutHeight + 700;
            // $('#onas').css('height', heightONAS);

        } else {
            $('#topCovers > .coverImg').css('width', '610px');
            $('#topCovers > .coverImg').css('height', '100%');
            $('#bottomCovers > .coverImg').css('width', '400px');

        }
        $('#showMobileMenu').click(function() {
            if ($('.menu-header-menu-container').hasClass('s')) {
                $('.menu-header-menu-container').css('display', 'none');
                $('.menu-header-menu-container').removeClass('s');
            } else {
                $('.menu-header-menu-container').css('display', 'block');
                $('.menu-header-menu-container').addClass('s');
            }
        });
        if (winH >= winW) {

            $('.menu-header-menu-container').css('display', 'none');


        } else {
            $('.menu-header-menu-container').css('display', 'block');
        }
        if (winW > 1040) {
            $('.contactBox').css('width', cntctW);
        } else {
            $('.contactBox').css('width', '100%');
        }
        $('span.your-name').parent('p').css({
            width: '48%',
            float: 'left'
        });
        $('span.your-email').parent('p').css({
            width: '48%',
            float: 'right'
        });
        $('div.aboutBg > img').css('width', winW);
        $('#covers').css('height', '830px');

        $('.JBG').css('height', $('#Jezus').height());

        
    }

    $('.cover').hover(function() {
        $(this).css('display', 'none');
        $(this).parents().children('a').children('.hoverCover').css('display', 'block');
    }, function() {});

    $('.hoverCover').hover(function() {}, function() {
        $(this).css('display', 'none');
        $(this).parents().parents().children('.cover').css('display', 'block');
    });

    function setSliderSize() {
        $('div.siema > div > div > img').css('left', $(window).width() / 2 - 960);
    }

    $(function() {
        $("#onas").tabs();
    });
    $(window).resize(function() {
        window.setTimeout(doStuff,500);
        function doStuff() {
            coversSetWidth();
            setSliderSize();
            setAboutBgHeight();
        }
    });

    coversSetWidth();
    setSliderSize();

    $(window).load(function() {
        $('.loader').css('display', 'none');
    });
});
