		<!-- W3TC-include-css -->
		<?php wp_footer(); ?>
		<?php $temp_res = get_template_directory_uri(); ?>
		<script src="<?php echo esc_url( $temp_res . '/js/jquery-3.2.1.min.js"' ); ?>"></script>
		<script src="<?php echo esc_url( $temp_res . '/js/script.js"' ); ?>"></script>
		<script src="<?php echo esc_url( $temp_res . '/bootstrap/js/bootstrap.min.js"' ); ?>"></script>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	</body>
</html>
