<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php bloginfo("name"); ?></title>
		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<link rel="stylesheet" href="/css/main.css">
		<?php wp_head(); ?>
	</head>
	<body>
		<div id="wrapper">
		<header>
			<div id="logo">
				<a href="/">
					<?php
						if ( function_exists( 'the_custom_logo' ) ) {
							the_custom_logo();
						}
 					?>
				</a>
			</div>
			<nav class="header_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
			<img id="showMobileMenu" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/menu.png"/>
		</header>
