<?php
/**
 * Template Name: Podstrona
 *
 * @package WordPress
 */
 ?>
 <html <?php language_attributes(); ?>>
 	<head>
 		<title><?php wp_title(); ?></title>
 		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
 		<?php wp_head(); ?>
 	</head>
 	<body>
        <header>
            <div id="logo">
				<a href="/">
					<img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/logo-m-1.png" alt="KZBytów" />
				</a>
			</div>
			<nav class="header_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
			</nav>
			<img id="showMobileMenu" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/menu.png"/>
		</header>
            <div id="menuBottomLine"></div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="submenu">
                    <a href="http://www.kosciolnowezycie.pl#kosciol">Kościół</a> > <?php the_title(); ?>
                </div>
                <div class="SlideSubPage">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div id="subPageContent">
                <h2><?php the_field('title1'); ?></h2>
                <p class="subheading">
                    <?php the_field('subtitle1'); ?>
                </p>
                <br style="clear: both">
                <div class="subSection">
                    <div class="leftColumnSubPage">
                        <?php the_field('leftcolumn'); ?>
                    </div>
                    <div class="rightColumnSubPage">
                        <iframe width="560" height="315" src="<?php the_field('film'); ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <br style="clear: both">
                <h2><?php the_field('title2'); ?></h2>
                <p class="subheading">
                    <?php the_field('subtitle2'); ?>
                </p>
                <?php
                if(is_page(240)){
                    ?>
                    <div class="leftColumnSK">
                        <h3>20+</h3>
                        <p>
                            Grupa przeznaczona dla młodych kobiet, na której jest czas na pogaduchy, dobrą kawę, babskie sprawy (np. wspólne gotowanie, mini-spa, warsztaty komunikacji) oraz wspólne czytanie Pisma Świętego i rozmowy o Bogu.
                        </p>
                        <h4>Miejsce</h4>
                        <p>
                            Spotykamy się w naszych domach lub w kawiarniach, czasem mamy grupy wyjazdowe. Żeby być na bieżąco z tym gdzie, co i jak - obserwuj nasz profil <a href="https://www.facebook.com/kzbytow/">Facebook</a>.
                        </p>
                        <h4>Czas</h4>
                        <p>
                            Spotykamy się zazwyczaj o 18:00 w co drugą środę miesiąca, aby dowiedzieć się kiedy i gdzie odbędzie się najbliższa grupa - <a href="mailto:kontakt@kzbytow.pl">napisz do nas</a> lub sprawdź na naszym profilu <a href="https://www.facebook.com/kzbytow/">Facebook</a>.
                        </p>
                    </div>
                    <div class="rightColumnSK">
                        <h3>50+</h3>
                        <p>
                            Grupa kobiet dojrzałych, na której każda kobieta może poczuć się akceptowana. Jest czas na lżejsze i głębsze tematy, wspólne spędzanie czasu przy kawie i cieście, Pismo Święte i rozmowy na temat Boga.
                        </p>
                        <h4>Miejsce</h4>
                        <p>
                            Spotykamy się w naszych domach lub w kawiarniach, czasem mamy grupy wyjazdowe. Żeby być na bieżąco z tym gdzie, co i jak - obserwuj nasz profil <a href="https://www.facebook.com/kzbytow/">Facebook</a>.
                        </p>
                        <h4>Czas</h4>
                        <p>
                            Spotykamy się zazwyczaj o 18:00 w co drugą środę miesiąca, aby dowiedzieć się kiedy i gdzie odbędzie się najbliższa grupa - <a href="mailto:kontakt@kzbytow.pl">napisz do nas</a> lub sprawdź na naszym profilu <a href="https://www.facebook.com/kzbytow/">Facebook</a>.
                        </p>
                    </div>
                    <?php

                }
                elseif (is_page(350)) {
                    ?>
                        <div class="mlodziezowkaText">
                            <img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/znakazpytania.png" />
                            <p>
                                Jesteś <span>nastolatkiem</span> i chcesz fajnie spędzić piątkowe popołudnia z <span>super</span> ludźmi w <span>najlepszych</span> klimatach? Zastanawiasz się na jaki <span>obóz</span> pojechać <span>latem</span>? Chcesz jeździć na chrześcijańskie imprezy z <span>przyjaciółmi</span>?
                            </p>
                            <img src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/dolacz-do-nas.png" />
                        </div>
                    <?php
                }
                elseif (is_page(346)) {
                    ?>
                        <div class="dzieciBoxes">
                            <div id="dBox1" class="dBox">
                                <div class="dTitle">
                                    GRUPA 1
                                </div>
                                <div class="dContent">
                                    <h4>
                                        Szkoła <br/>podstawowa
                                    </h4>
                                    <p>
                                        klasy I - III
                                    </p>
                                </div>
                            </div>
                            <div id="dBox2" class="dBox">
                                <div class="dTitle">
                                    GRUPA 2
                                </div>
                                <div class="dContent">
                                    <h4>
                                        Szkoła <br/>podstawowa
                                    </h4>
                                    <p>
                                        klasy IV - VI
                                    </p>
                                </div>
                            </div>
                            <div id="dBox3" class="dBox">
                                <div class="dTitle">
                                    GRUPA 3
                                </div>
                                <div class="dContent">
                                    <h4>
                                        Gimnazjum
                                        <br/>
                                        <br/>
                                    </h4>
                                    <p>
                                        klasy I - III
                                    </p>
                                </div>
                            </div>
                            <div id="dBox4" class="dBox">
                                <div class="dTitle">
                                    GRUPA 4
                                </div>
                                <div class="dContent">
                                    <h4>
                                        Szkoła ponadgimnazjalna
                                    </h4>
                                    <p>
                                        klasy I - III
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php
                }
                ?>
                <br style="clear: both">
                <h2><?php the_field('title3'); ?></h2>
                <p class="subheading">
                    <?php the_field('subtitle3'); ?>
                </p>
                <?php
                    $top_posts = new WP_Query( "page_id=19" );

                    if ( $top_posts->have_posts() ) :
                        $i = 1;
                        while ( $top_posts->have_posts() ) : $top_posts->the_post();
                            ?>
                                <div class="subPageForm">
                                    <?php the_field('form'); ?>
                                </div>
                            <?php
                            $i++;
                        endwhile;
                    endif;
                ?>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
<?php get_footer(); ?>
