<?php get_header(); ?>
    <div id="main">
        <img id="mobileSlide" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/SLIDE1-kosciol.jpg"/>
        <div class="siema">
            <?php
                $args = array('post_type' => 'top_slider', 'order' => 'ASC');

                $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                if ( $top_posts->have_posts() ) :
                    $i = 1;
                    while ( $top_posts->have_posts() ) : $top_posts->the_post();

                        ?>
                            <?php the_post_thumbnail(); ?>
                        <?php
                        $i++;
                    endwhile;
                endif;
            ?>
        </div>
        <div id="content">
            <section id="kosciol">
                <h2>Kościół</h2>
                <p class="subheading">
                    Dla każdego jest miejsce, wybierz coś dla siebie
                </p>
                <div id="topCovers">
                    <div class="coverImg">
                        <img class="cover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/NABO-1.jpg" alt="Nabożeństwa" />
                        <a href="http://www.kosciolnowezycie.pl/nabozenstwa/">
                            <img class="hoverCover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/NABO-hover2-1.jpg" alt="Nabożeństwa" />
                        </a>
                    </div>
                    <div class="coverImg">
                        <img class="cover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/MLODZIEZ.jpg" alt="Grupa Młodzieżowa" />
                        <a href="http://www.kosciolnowezycie.pl/mlodziezowka/">
                            <img class="hoverCover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/MLODZIEZ-hover2.jpg" alt="Nabożeństwa" />
                        </a>
                    </div>
                </div>
                <div id="bottomCovers">
                    <div class="coverImg">
                        <img class="cover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/dzieci.jpg" alt="Dzieci" />
                        <a href="http://www.kosciolnowezycie.pl/dzieci/">
                            <img class="hoverCover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/DZIECI-hover2.jpg" alt="Nabożeństwa" />
                        </a>
                    </div>
                    <div class="coverImg">
                        <img class="cover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/kobiety.jpg" alt="Strefa Kobiet" />
                        <a href="http://www.kosciolnowezycie.pl/strefa-kobiet/">
                            <img class="hoverCover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/kobiety-hover2.jpg" alt="Nabożeństwa" />
                        </a>
                    </div>
                    <div class="coverImg">
                        <img class="cover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/mezczyzn.jpg" alt="Grupa Mężczyzn" />
                        <a href="http://www.kosciolnowezycie.pl/grupa-mezczyzn/">
                            <img class="hoverCover" src="http://www.kosciolnowezycie.pl/wp-content/uploads/2016/05/mezczyzn-hover2.jpg" alt="Nabożeństwa" />
                        </a>
                    </div>
                </div>
            </section>
            <section id="onas">
                <h2>O nas</h2>
                <ul>
                    <?php
                        $args = array('post_type' => 'o_nas', 'order' => 'ASC');

                        $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                        if ( $top_posts->have_posts() ) :
                            $i = 1;
                            while ( $top_posts->have_posts() ) : $top_posts->the_post();

                                ?>
                                    <li class="aboutTab"><a href="#about<?php echo $i ?>"><?php the_title(); ?></a></li>
                                <?php
                                $i++;
                            endwhile;
                        endif;
                    ?>
                </ul>
                <?php
                    $args = array('post_type' => 'o_nas', 'order' => 'ASC');

                    $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                    if ( $top_posts->have_posts() ) :
                        $i = 1;
                        while ( $top_posts->have_posts() ) : $top_posts->the_post();

                            ?>
                                <div class="about_element" id="about<?php echo $i ?>">
                                    <div class="aboutBg" style="background:url('<?php the_post_thumbnail_url(); ?>')">

                                    </div>
                                    <div class="aboutTitle">
                                        <?php the_meta(); ?>
                                    </div>
                                    <div class="aboutText">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            <?php
                            $i++;
                        endwhile;
                    endif;
                ?>
            </section>
            <section id="news">
                <h2>Video</h2>
                <p class="subheading">
                    Obejrzyj nas na YouTube
                </p>
                <div class="ytFilms">
                    <?php
                        $args = array('post_type' => 'news', 'order' => 'ASC', 'posts_per_page' => 3);

                        $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                        if ( $top_posts->have_posts() ) :
                            $i = 1;
                            while ( $top_posts->have_posts() ) : $top_posts->the_post();
                                ?>
                                <iframe width="560" height="315" src="<?php the_field('link'); ?>" frameborder="0" allowfullscreen></iframe>
                                <?php
                                $i++;
                            endwhile;
                        endif;
                    ?>
                </div>
                <br style="clear: both">
            </section>
            <section id="kazania">
                <h2>Kazania</h2>
                <p class="subheading">
                    Posłuchaj najnowszych nagrań z niedzielnego nabożeństwa
                </p>
                <div class="ytFilms">
                    <?php
                        $args = array('post_type' => 'kazania', 'order' => 'DESC');

                        $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                        if ( $top_posts->have_posts() ) :
                            $i = 1;
                            while ( $top_posts->have_posts() ) : $top_posts->the_post();

                                ?>
                                    <iframe width="560" height="315" src="<?php the_field('link'); ?>" frameborder="0" allowfullscreen></iframe>
                                <?php
                                $i++;
                            endwhile;
                        endif;
                    ?>
                </div>
                <br style="clear: both">
                <a target="_blank" class="ytLink" href="https://www.youtube.com/user/kzbytow">...więcej</a>
            </section>
            <section id="Jezus">
                <div class="JBG">
                    
                </div>
                <h2>Jezus</h2>
                <p class="subheading">
                    Najlepsza wiadomość, jaką możesz usłyszeć
                </p>
                <div id="jContent">
                    <?php
                        $top_posts = new WP_Query( "page_id=19" );

                        if ( $top_posts->have_posts() ) :
                            $i = 1;
                            while ( $top_posts->have_posts() ) : $top_posts->the_post();
                                ?>
                                    <?php the_content(); ?>
                                    <div id="leftJ">
                                        <?php the_field('tekst'); ?>
                                    </div>
                                    <div id="rightJ">
                                        <?php the_field('form'); ?>
                                    </div>
                                <?php
                                $i++;
                            endwhile;
                        endif;
                    ?>
                </div>

            </section>
            <section id="galeria">
                <h2>Galeria</h2>
                <p class="subheading">
                    Kliknij w okładkę albumu, żeby zobaczyć więcej zdjęć
                </p>
                <div id="galleryBox">
                    <?php
                        $args = array('post_type' => 'galeria', 'order' => 'DESC', 'posts_per_page' => 10);

                        $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                        if ( $top_posts->have_posts() ) :
                            $i = 1;
                            while ( $top_posts->have_posts() ) : $top_posts->the_post();

                                ?>
                                    <a class="galleryItemA" href="<?php the_field('link'); ?>">
                                        <div class="galleryItem">
                                            <?php the_post_thumbnail(); ?>
                                            <div class="galleryItemTitle">
                                                <?php the_title(); ?>
                                            </div>
                                        </div>
                                    </a>
                                <?php
                                $i++;
                            endwhile;
                        endif;
                    ?>

                </div>
                <a style="text-align:center;display:block;" href="http://kosciolnowezycie.pl/album/">...więcej</a>
            </section>
            <section id="kontakt">
                <h2>Kontakt</h2>
                <p class="subheading">
                    Przyjdź, napisz, zadzwoń - czekamy na Ciebie
                </p>
                <div id="leftContact" class="contactBox">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2335.6794359902087!2d17.486054316264934!3d54.1680419801595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47020632851c213f%3A0x7a9be92a18528692!2zTcWCecWEc2thIDYsIDg0LTM0MiBCeXTDs3c!5e0!3m2!1spl!2spl!4v1463920845108" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <?php
                    $args = array('post_type' => 'stopka', 'order' => 'ASC');

                    $top_posts = new WP_Query( apply_filters( 'et_popular_posts_tabs_query_args', $args ) );

                    if ( $top_posts->have_posts() ) :
                        $i = 1;
                        while ( $top_posts->have_posts() ) : $top_posts->the_post();

                            ?>
                            <div id="centerContact" class="contactBox">
                                <h3>Adres kościoła</h3>
                                <p>
                                    <?php the_field('adres_kosciola'); ?>
                                </p>
                                <h3>Email</h3>
                                <p>
                                    <a style="text-decoration:none; color:#6A6A6A;" href="mailto:<?php the_field('email1'); ?>"><?php the_field('email1'); ?></a><br/><br/>
                                    <a style="text-decoration:none; color:#6A6A6A;" href="mailto:<?php the_field('email2'); ?>"><?php the_field('email2'); ?></a>
                                </p>
                                <h3>Telefon</h3>
                                <p>
                                    <?php the_field('telefon'); ?>
                                </p>
                                <p>
                                    <span style="color:#000;">NUMER KONTA BANKOWEGO (BGŻ):</span><br/>
                                    90 2030 0045 1110 0000 0247 4410
                                </p>
                            </div>
                            <div id="rightContact" class="contactBox">
                                <h3>Życie kościoła</h3>
                                <p>
                                    <?php the_field('zycie_kosciola'); ?>
                                    <a target="_blank" href="https://www.facebook.com/kzbytow">FACEBOOK</a><span>|</span><a target="_blank" href="https://www.youtube.com/channel/UCxiCpDBJ3Zo_jPBNrRehnXA">YOUTUBE</a>
                                </p>
                            </div>
                            <?php
                            $i++;
                        endwhile;
                    endif;
                ?>

            </section>
        </div>
    </div>
    <div id="delimiter">
    </div>
<?php get_footer(); ?>
<script type="text/javascript">
$(document).ready(function() {
    //SIEMA.JS

    const siema = new Siema({
        selector: '.siema',
        duration: 300,
        easing: 'ease-out',
        perPage: 1,
        startIndex: 0,
        draggable: true,
        threshold: 20,
        loop: true,
    });

    setInterval(() => siema.next(), 2000);

});
</script>
