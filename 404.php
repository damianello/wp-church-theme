 <html <?php language_attributes(); ?>>
 	<head>
 		<title><?php wp_title(); ?></title>
 		<meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
 		<?php wp_head(); ?>
 	</head>
 	<body>
        <header>
			<div id="logo">
				<a href="/">
					<img src="http://www.kzbytow.pl/wp-content/uploads/2016/05/logo-m-1.png" alt="KZBytów">
				</a>
			</div>
			<nav class="header_menu">
				<div class="menu-header-menu-container">
                    <ul id="menu-header-menu" class="menu">
                        <li id="menu-item-117" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-117"><a href="http://kzbytow.pl#kosciol">Kościół</a></li>
                        <li id="menu-item-118" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-118"><a href="http://kzbytow.pl#onas">O nas</a></li>
                        <li id="menu-item-120" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-120"><a href="http://kzbytow.pl#news">News</a></li>
                        <li id="menu-item-121" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-121"><a href="http://kzbytow.pl#kazania">Kazania</a></li>
                        <li id="menu-item-122" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-122"><a href="http://kzbytow.pl#Jezus">Jezus</a></li>
                        <li id="menu-item-123" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-123"><a href="http://kzbytow.pl#galeria">Galeria</a></li>
                        <li id="menu-item-124" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-124"><a href="http://kzbytow.pl#kontakt">Kontakt</a></li>
                        <li id="menu-item-372" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-372"><a target="_blank" href="https://www.facebook.com/kzbytow/">FB</a></li>
                        <li id="menu-item-373" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-373"><a target="_blank" href="https://www.youtube.com/channel/UCxiCpDBJ3Zo_jPBNrRehnXA">YT</a></li>
                        </ul>
                    </div>
                </nav>
		</header>
        <?php get_footer(); ?>